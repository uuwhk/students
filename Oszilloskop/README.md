<img src="../figures/Logo_KIT.svg" width="200" style="float:right;" />

# Fakultät für Physik

## Physikalisches Praktikum P1 für Studierende der Physik

Versuch P1-31, 32, 33 (Stand: November 2023)

[Raum F1-15](https://labs.physik.kit.edu/img/Praktikum/Lageplan_P1.png)



# Oszilloskop

## Motivation

Im Laufe dieses Versuchs haben Sie Gelegenheit sich mit einem der wichtigsten Werkzeuge zur Visualisierung physikalischer Prozesse in Form von veränderlichen, elektrischen Signalen vertraut zu machen: dem Oszilloskop. Ein *analoges* Oszilloskop besteht aus einer [Kathodenstrahlröhre](https://de.wikipedia.org/wiki/Kathodenstrahlr%C3%B6hre) mit zwei senkrecht zueinander positionierten Paaren von Kondensatorplatten, zur elektrischen Ablenkung eines erzeugten Elektronenstrahls. Der Elektronenstrahl wird schließlich auf einem phosphoreszierenden Bildschirm sichtbar gemacht. 

Zunächst untersucht man mit dem Oszilloskop *ein* veränderliches Signal auf der vertikalen ($y$-)Achse, als Funktion der Zeit, auf der horizontalen ($x$-)Achse. Bei analogen Geräten wird die Zeitbasis durch eine ansteigende Sägezahnspannung realisiert, die die Anzeige mit konstanter Geschwindigkeit von links nach rechts ablenkt. Das zu untersuchende elektrische Signal durchläuft zunächst eine Verstärkung und wird dann als Ablenkung entlang der $y$-Achse dargestellt. Es ist notwendig dem Gerät anzuzeigen wann die Aufzeichnung des Signals beginnen soll. Dies erfolgt durch eine Auslösebedingung, [*Trigger*](https://de.wikipedia.org/wiki/Trigger_(Physik)) genannt, die in den einfachsten Fällen darin besteht, dass das Signal eine zuvor festgelegte Schwelle überschreitet. 

Jedes Oszilloskop besitzt im allgemeinen die folgenden **Grundeigenschaften**: 

- **Eingangssignale:** Jeweils einen auf der $x$- und $y$-Achse in Position und Amplitude unabhängig darstellbaren Eingang, für zwei Signale (CH1 und CH2). CH1 und CH2 können im *Einkanalbetrieb* jeweils einzeln, im *Zweikanalbetrieb* auf der gleichen Zeitbasis, gemeinsam angezeigt werden. 
- **Signaleinkopplung:** Jedes Signal kann im Gleichstrom (DC-) oder Wechselstrom (AC-)Modus eingekoppelt werden. Üblicherweise gibt es auch eine Ground (GRD-)Anzeige. Die GRD-Anzeige zeigt die Nulllage des Verstärkers am jeweiligen Signaleingang an. Die DC-Einkopplung zeigt das Signal an, wie es nach der Verstärkung auf dem jeweiligen Eingang (z.B. mit einem vermeintlichen *offset*) anliegt. Die AC-Einkopplung zeigt das jeweilige Signal relativ zu einem unveränderlichen Anteil an. Dabei werden alle DC-Anteile (wie *offsets*) durch einen in Serie geschalteten Kondensator [ausgefiltert](https://de.wikipedia.org/wiki/Tiefpass).
- **Trigger:** Ein eingebauter Trigger für jeden Kanal, der jeweils zusätzlich zur regelbaren Schwelle berücksichtigen kann, ob das Signal für den Start der Aufzeichnung mit der Zeit größer (*steigende Flanke*) oder kleiner (*fallende Flanke*) werden soll. Ein dritter Eingang (EXT TRIG) erlaubt den Anschluss eines externen Triggersignals zum Auslösen der Messung. 
- **$XY$-Betrieb:** Im *XY-Betrieb* kann die konstante Ablenkung der Anzeige auf der $x$-Achse durch das Signal auf CH1 ersetzt werden, während das Signal auf CH2 auf der $y$-Achse dargestellt wird. Dieser Betrieb kann z.B. zur Aufzeichnung von [Lissajous-Figuren](https://de.wikipedia.org/wiki/Lissajous-Figur) genutzt werden.  

Heutzutage werden Oszilloskope bis auf wenige Ausnahmen digital betrieben, bis hin zu Oszilloskopen, wie dem [Picoscope](https://www.picotech.com/products/oscilloscope) des Vertreibers Pico Technology, die auch im Praktikum in Verwendung sind. Diese Oszilloskope werden über eine USB-Schnittstelle angesteuert und ausgelesen und verfügen selbst über keinen Bildschirm mehr. Sie sind daher auf den ersten Blick zuweilen gar nicht als Oszilloskope zu erkennen. In ihrer Funktionsweise unterscheiden sich digitale und analoge Oszilloskope, trotz gleicher Grundeigenschaften, teilweise erheblich, was zur Folge hat, dass die eine Bauart gegenüber der anderen jeweils Stärken und Schwächen aufweist. Manche Oszilloskope besitzen entsprechende Zusatzfunktionen. **In den oben aufgeführten Grundeigenschaften sind jedoch alle Oszilloskope gleich.**      

## Lehrziele

Wir listen im Folgenden die wichtigsten **Lehrziele** auf, die wir Ihnen mit dem Versuch **Oszilloskop** vermitteln möchten: 

- Sie führen eine Reihe von Messungen mit Oszilloskopen durch im Rahmen derer Sie die Anwendung von Oszilloskopen als Messwerkzeugen einüben. Sie verinnerlichen dabei die Grundeigenschaften eines Oszilloskops, als einem der wichtigsten Werkzeuge eines:r Physikers:in, um veränderliche, elektrische Signale sichtbar zu machen. 
- Sie machen sich im Rahmen der Vorbereitung auf den Versuch mit den, trotz identischer Grundeigenschaften, grundsätzlich verschiedenen Funktionsweisen analoger und digitaler Oszilloskope vertraut. Einige der Aufgaben weisen Ihnen die Grenzen des einen oder anderen Oszilloskop-Typs auf. 
- Bei diesem Versuch sollte Ihr Augenmerk bei der Bearbeitung der Aufgaben vor allem auf dem Einsatz und dem Vergleich der beiden Oszilloskop-Typen liegen. Achten Sie auf die Gemeinsamkeiten und die teilweise offensichtlichen, teilweise subtilen Unterschiede. Die Messungen, die Sie zum Teil auch in anderen P1-Versuchen durchführen dienen hier vor allem als Medium, um sich die Verwendung des Oszilloskops nachhaltig bewusst zu machen.

## Versuchsaufbau

Im Folgenden sind die verwendeten Aufbauten und Apparaturen beschrieben, die Sie im Verlauf dieses Versuchs verwenden. Eine Auflistung der für ihre Auswertung wichtigsten Apparaturen und deren Eigenschaften finden Sie in der Datei [Datenblatt.md](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/tree/main/Oszilloskop/Datenblatt.md).

Die über weite Teile des Versuchs verwendeten Signale werden mit Hilfe von zwei Frequenzgeneratoren erzeugt. Mit Hilfe einer kleinen Stecktafel, und verschiedener elektrischer Bauelemente stecken Sie einfache Schaltungen, die Sie sowohl mit einem digitalen als auch mit einem analogen Oszilloskop untersuchen. Zu den Bauteilen zählen Widerstände, Kondensatoren, Spulen, eine Silizium (Si-)[Diode](https://de.wikipedia.org/wiki/Diode) und eine [Z-Diode](https://de.wikipedia.org/wiki/Z-Diode). 

<img src="./figures/OszilloskopAufbau.png" width="900" style="zoom:100%;" />

Die Aufgaben dieses Versuchs dienen dazu, Sie an die Arbeit mit dem Oszilloskop heranzuführen. Zum Teil lernen Sie dabei die Grenzen des einen oder anderen Oszilloskops kennen. Beachten Sie jedoch, dass beide Oszilloskop-Typen in ihren Grundeigenschaften gleich sind.

## Wichtige Hinweise zum Versuch

- Oszilloskope kommen in verschiedener Gestalt, z.B. in den folgenden Versuchen vor: 
  - "[Ferromagnetische Hysterese](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/tree/main/Ferromagnetische_Hysterese)" (hier z.B. prominent im $XY$-Betrieb in Verwendung) , 
  - "[Lichtgeschwindigkeit](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/tree/main/Lichtgeschwindigkeit)"(hier sowohl im Zweikanal- als auch im $XY$-Betrieb in Verwendung), 
  - "[Vierpole und Leitungen](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/tree/main/Vierpole_und_Leitungen)" (aus diesem Versuch sind auch einige einfache Schaltungen, wie das Differenzier- und Integrierglied, sowie der Phasenschieber entlehnt), 
  - Im Versuch "[Spezifische Ladung des Elektrons](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/tree/main/Spezifische_Ladung_des_Elektrons)" können Sie sich eine ausgebaute und umfunktionierte [Kathodenstrahlröhre](https://de.wikipedia.org/wiki/Kathodenstrahlr%C3%B6hre) eines analogen Oszilloskops anschauen, die dort für die [Bestimmung der spezifischen Ladung des Elektrons nach dem Verfahren von Busch](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/blob/main/Spezifische_Ladung_des_Elektrons/doc/Hinweise-Aufgabe-2.md) verwendet wird.


# Navigation

Das P1 befindet sich derzeit in der Umstellung zu verbesserten Anleitungen, die alle notwendigen Informationen zur Vorbereitung auf den Versuch beinhalten. 

**Dieser Versuch wird vorläufig noch nach der alten Anleitung durchgeführt.** 

- Abbildungen zum Versuch finden Sie im Verzeichnis [`figures`](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/tree/main/Oszilloskop/figures). 
- Informationen zur Vorbereitung auf den Versuch nach der alten Anleitung finden Sie im Verzeichnis [`doc`](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/tree/main/Oszilloskop/doc). 

Im Verzeichnis [`doc`](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/tree/main/Oszilloskop/doc) finden Sie auch die originale Version der alten Anleitung, die z.T. noch etwas mehr Information enthält, als die Jupyter-notebook Version. In einigen Fällen weichen Nummerierungen und Reihenfolge der Versuchsteile in den alten Anleitungen von der jeweiligen Version in Jupyter-notebook ab. In diesen Fällen ist die jeweilige Version der **Durchführung in Jupyter-notebook** für Sie maßgeblich! 
